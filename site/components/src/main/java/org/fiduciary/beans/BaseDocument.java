package org.fiduciary.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="fiduciary:basedocument")
public class BaseDocument extends HippoDocument {

}
